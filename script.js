/*
1) Чому для роботи з input не рекомендується використовувати клавіатуру?

Натискання клавіш може спрацьовувати на будь-якому полі вводу, різні браузери можуть має різні способи обробки
клавіатурних подій, і код може вести себе по-різному в різних браузерах.
*/

const buttons = document.querySelectorAll('.btn');

document.addEventListener('keydown', (event) => {
    const keyPressed = event.key.toUpperCase();

    buttons.forEach(button => {
        if (button.getAttribute('data-key') === keyPressed || button.textContent.toUpperCase() === keyPressed) {
            button.classList.add('blue');
        } else {
            button.classList.remove('blue');
        }
    });
});
